import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class SwapiService {
    protected static RequestSpecification swapiServiceReqSpec() {
        return new RequestSpecBuilder()
                .setBaseUri("http://swapi.dev/api")
                .build();
    }

    protected static ResponseSpecification swapiServiceValidResSpec() {
        return new ResponseSpecBuilder()
                .expectStatusCode(200)
                .build();
    }
}
