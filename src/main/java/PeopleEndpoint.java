import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class PeopleEndpoint extends SwapiService {
    private static RequestSpecification filmsEndpointReqSpec() {
        return new RequestSpecBuilder()
                .addRequestSpecification(swapiServiceReqSpec())
                .setBasePath("/people")
                .build();
    }
}
