import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class FilmEndpoint extends SwapiService{

    private static RequestSpecification filmsEndpointReqSpec() {
        return new RequestSpecBuilder()
                .addRequestSpecification(swapiServiceReqSpec())
                .setBasePath("/films")
                .build();
    }

    public static ValidatableResponse getFilms() {
        ValidatableResponse response =
                given()
                    .spec(filmsEndpointReqSpec())
                .when()
                    .get()
                .then()
                    .spec(swapiServiceValidResSpec());

        return response;
    }

}
