import io.restassured.response.ValidatableResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SwapiTests {

    ValidatableResponse response;

    @BeforeClass
    public void getFilms() {
        response = FilmEndpoint.getFilms();
    }

    @Test
    public void requestFilms_checksFirstFilmTitle() {
        String actualTitle = response.extract().path("results[0].title");
        Assert.assertEquals(actualTitle, "A New Hope");
    }

    @Test
    public void requestFilms_checksCountIsSix(){
        int actualCount = response.extract().path("count");
        Assert.assertEquals(actualCount, 6);
    }
}
